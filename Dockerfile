FROM rocker/r-ver:4.4

ARG QUARTO_VER
RUN apt-get update -y && apt-get install -y libcurl4-openssl-dev curl openssl libssl-dev libxml2-dev zlib1g-dev && apt-get clean
RUN curl -LOs https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VER}/quarto-${QUARTO_VER}-linux-amd64.deb && dpkg -i quarto-${QUARTO_VER}-linux-amd64.deb && rm -f *deb
RUN Rscript -e 'install.packages("renv")'

ADD renv.lock /
RUN Rscript -e 'library("renv"); renv::restore()'
ENTRYPOINT /bin/bash
